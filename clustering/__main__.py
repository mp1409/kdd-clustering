""" Command line interface for the clusterings package. """

import argparse
import sys

import kmeans
import utils


parser = argparse.ArgumentParser(description='Clustering algorithms for KDD.')
parser.add_argument('algorithm', help='the clustering algorithm to use')
parser.add_argument('datafile', help='the csv file with the data')

args = parser.parse_args()

if args.algorithm == 'kmeans':
    clustering = utils.read_points_from_csv(args.datafile)
    utils.print_clustering(clustering, description='Initial:')

    clustering = kmeans.run(clustering, callback=utils.print_clustering)
    utils.print_clustering(clustering, description='Finished:')
else:
    print('Unknown algorithm: ' + args.algorithm, file=sys.stderr)
    sys.exit(1)
