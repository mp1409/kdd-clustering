"""
Implementation of the k-means algorithm.

A specific clustering stored as a dictionary that maps the name of the cluster
to a list of points.
"""

import collections
from statistics import mean

from utils import Point, compare_clusterings, euclid


def calculate_centroids(clustering):
    """
    Return the centroids for a clustering as a dictionary mapping the name of
    the cluster to the centroid point.
    """
    centroids = {}

    for name, points in clustering.items():
        mean_x = mean(p.x for p in points)
        mean_y = mean(p.y for p in points)

        centroids[name] = Point('Centriod ' + name, mean_x, mean_y)

    return centroids


def recluster(clustering):
    """ Run a single round of k-means and return a new clustering. """
    newclustering = collections.defaultdict(list)
    centroids = calculate_centroids(clustering)

    for points in clustering.values():
        for point in points:
            mincluster = None
            mindistance = float('inf')

            for name in clustering:
                distance = euclid(point, centroids[name])

                if distance < mindistance:
                    mincluster = newclustering[name]
                    mindistance = distance

            mincluster.append(point)

    return newclustering


def run(clustering, callback=None):
    """ Run the k-means algorithm to completion. """
    done = False
    step = 0

    while not done:
        newclustering = recluster(clustering)
        done = compare_clusterings(clustering, newclustering)
        step += 1
        clustering = newclustering

        if callback is not None:
            callback(clustering, 'Step {}:'.format(step))

    return clustering
