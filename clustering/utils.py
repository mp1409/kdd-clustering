""" Algorithm-independent clustering utilities. """

import collections
import csv
import math

Point = collections.namedtuple('Point', ['name', 'x', 'y'])


def read_points_from_csv(path):
    """ Read the points and their initial clustering from a CSV file. """
    with open(path) as file:
        reader = csv.DictReader(file, skipinitialspace=True, strict=True)

        clustering = collections.defaultdict(list)

        for row in reader:
            p = Point(row['Name'], float(row['x']), float(row['y']))
            clustering[row['Initial cluster']].append(p)

        return clustering


def euclid(p1, p2):
    """ Return the Euclidean distance of points p1 and p2. """
    return math.sqrt((p2.x - p1.x)**2 + (p2.y - p1.y)**2)


def compare_clusterings(c1, c2):
    """ Return True if clusterings c1 and c2 are equal. """
    if c1.keys() != c2.keys():
        return False

    for name in c1:
        if set(c1[name]) != set(c2[name]):
            return False

    return True


def print_clustering(clustering, description=None):
    """ Print a clustering to stdout, preceeded by an optional description. """

    if description is not None:
        print(description)

    for name, points in clustering.items():
        pointstring = ', '.join(p.name for p in points)
        print('{}: {{{}}}'.format(name, pointstring))

    print()
