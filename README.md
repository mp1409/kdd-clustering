# KDD-Clustering
Clustering algorithms for KDD. Requires Python 3.

## How to use
```
$ git clone https://mp1409@gitlab.com/mp1409/kdd-clustering.git
$ cd kdd-clustering
$ python3 clustering kmeans sampledata/kmeans.csv
Initial:
C1: {P1, P2, P3, P4}
C2: {P5, P6, P7, P8}
C3: {P9, P10, P11, P12}

Finished:
C1: {P2, P7, P3, P9, P11}
C2: {P1, P4, P5, P6}
C3: {P8, P10, P12}
```

Instead of using git, you can also just [download the files](https://gitlab.com/mp1409/kdd-clustering/repository/archive.zip?ref=master) and unzip them ;-)
